package com.example.qrcodescanner;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.icu.text.SymbolTable;
import android.net.Uri;
import android.os.Bundle;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.ContactsContract;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import ezvcard.Ezvcard;
import ezvcard.VCard;

public class MainActivity extends AppCompatActivity {

    enum ResultState {
        TEXT,
        URL,
        CONTACT
    }

    private int REQUEST_PERMISSION_CAMERA = 1;
    private ResultState State;
    private Object ResultObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Ask the user for camera permission if needed
        if (!checkPermission(Manifest.permission.CAMERA)) {
            askForPermission(Manifest.permission.CAMERA);
            return;
        }

        startQRScan();
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private String getDateString() {
        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy hh:m aa, ");
        String dateString = format.format(new Date());
        return dateString;
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PermissionChecker.PERMISSION_GRANTED;
    }

    private void askForPermission(String permission) {
        boolean hasCameraPerm = checkPermission(permission);

        if (!hasCameraPerm) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_PERMISSION_CAMERA);
            }
        }
    }

    private void startQRScan() {
        IntentIntegrator scanIntegrator = new IntentIntegrator(this);
        scanIntegrator.setPrompt(" ");
        scanIntegrator.setBeepEnabled(true);
        scanIntegrator.setOrientationLocked(true);
        scanIntegrator.setBarcodeImageEnabled(false);
        scanIntegrator.initiateScan();
    }

    public void onScan(View view) {
        //Ask the user for camera permission if needed
        if (checkPermission(Manifest.permission.CAMERA) == false) {
            askForPermission(Manifest.permission.CAMERA);
            return;
        }

        startQRScan();
    }

    private void setTitleBar(String title, String subtitle, int icon) {
        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.titleFragment);
        View view = fragment.getView();

        TextView txtTitle = view.findViewById(R.id.txtTitle);
        TextView txtSubTitle = view.findViewById(R.id.txtSubTitle);
        ImageView imgIcon = view.findViewById(R.id.imgIcon);
        txtTitle.setText(title);
        txtSubTitle.setText(subtitle);
        imgIcon.setImageResource(icon);
    }

    private void setBody(String title, String subtitle) {
        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.bodyFragment);
        View view = fragment.getView();

        TextView txtTitle = view.findViewById(R.id.txtTitle);
        TextView txtSubTitle = view.findViewById(R.id.txtSubTitle);

        txtTitle.setText(title);
        txtSubTitle.setText(subtitle);
    }

    private void setFooter(final String result) {
        FragmentManager fragManager = getSupportFragmentManager();
        Fragment fragment = fragManager.findFragmentById(R.id.footerFragment);
        View view = fragment.getView();

        ImageView imgAction1 = view.findViewById(R.id.imgAction1);
        ImageView imgAction2 = view.findViewById(R.id.imgAction2);
        ImageView imgAction3 = view.findViewById(R.id.imgAction3);
        ImageView imgAction4 = view.findViewById(R.id.imgAction4);

        TextView txtAction1 = view.findViewById(R.id.txtAction1);
        TextView txtAction2 = view.findViewById(R.id.txtAction2);
        TextView txtAction3 = view.findViewById(R.id.txtAction3);
        TextView txtAction4 = view.findViewById(R.id.txtAction4);

        imgAction1.setVisibility(View.INVISIBLE);
        txtAction1.setVisibility(View.INVISIBLE);
        imgAction2.setVisibility(View.INVISIBLE);
        txtAction2.setVisibility(View.INVISIBLE);
        imgAction3.setVisibility(View.INVISIBLE);
        txtAction3.setVisibility(View.INVISIBLE);
        imgAction4.setVisibility(View.INVISIBLE);
        txtAction4.setVisibility(View.INVISIBLE);

        /*
        ----------- ResultState TEXT -----------
        Nothing displayed

        ----------- ResultState URL -----------
        1. Open Browser button

        ----------- ResultState CONTACT -----------
        1. Add Contact button

         */

        //Set icons for respective imageview
        if (State == ResultState.URL) {
            imgAction1.setImageResource(R.drawable.ic_baseline_open_in_browser_24);
            txtAction1.setText("Open Browser");
            imgAction1.setVisibility(View.VISIBLE);
            txtAction1.setVisibility(View.VISIBLE);
        }
        else if (State == ResultState.CONTACT) {
            imgAction1.setImageResource(R.drawable.ic_baseline_person_add_24);
            txtAction1.setText("Add Contact");
            imgAction1.setVisibility(View.VISIBLE);
            txtAction1.setVisibility(View.VISIBLE);
        }
        else if (State == ResultState.TEXT) {
            imgAction1.setImageResource(R.drawable.ic_baseline_assignment_64);
            txtAction1.setText("Copy to Clipboard");
            imgAction1.setVisibility(View.VISIBLE);
            txtAction1.setVisibility(View.VISIBLE);
        }

        //Click events
        imgAction1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //redirect to url on click
                if (State == ResultState.URL) {
                    showToast("Redirecting...");
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result));
                    startActivity(browserIntent);
                }

                //add contact on click
                else if (State == ResultState.CONTACT) {
                    VCard vcard = (VCard) ResultObj;
                    //Start contact creation intent
                    Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                    intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                    populateContactIntent(vcard, intent);
                    startActivity(intent);
                }

                //copy to clibaord
                else if (State == ResultState.TEXT) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("qr-code", result);
                    clipboard.setPrimaryClip(clip);
                    showToast("Copied to clipboard.");
                }

            }
        });
    }

    private String generateContactString(VCard vc) {
        StringBuilder builder = new StringBuilder();

        //First name + Last name
        if (vc.getStructuredName().getGiven() != null && vc.getStructuredName().getFamily() != null) {
            builder.append(vc.getStructuredName().getGiven() + " " + vc.getStructuredName().getFamily() + "\n");
        }
        //First name
        else if (vc.getStructuredName().getGiven() != null) {
            builder.append(vc.getStructuredName().getGiven() + "\n");
        }

        //Organisation
        if (vc.getOrganization() != null) {
            builder.append(vc.getOrganization().getValues().get(0) + "\n");
        }

        //Email
        if (vc.getEmails().size() >= 1) {
            builder.append(vc.getEmails().get(0).getValue() + "\n");
        }

        //Phone number
        if (vc.getTelephoneNumbers().size() >= 1) {
            builder.append(vc.getTelephoneNumbers().get(0).getText() + "\n");
        }

        //Address
        if (vc.getAddresses().size() >= 1) {
            builder.append(vc.getAddresses().get(0).getStreetAddressFull() + "\n");
        }

        return builder.toString().trim();
    }

    private void populateContactIntent(VCard vc, Intent intent) {
        //First name + Last name
        if (vc.getStructuredName().getGiven() != null && vc.getStructuredName().getFamily() != null) {
            intent.putExtra(ContactsContract.Intents.Insert.NAME, vc.getStructuredName().getGiven() + " " + vc.getStructuredName().getFamily());
        }
        //First name
        else if (vc.getStructuredName().getGiven() != null) {
            intent.putExtra(ContactsContract.Intents.Insert.NAME, vc.getStructuredName().getGiven());
        }

        //Organisation
        if (vc.getOrganization() != null) {
            intent.putExtra(ContactsContract.Intents.Insert.COMPANY, vc.getOrganization().getValues().get(0));
        }

        //Email
        if (vc.getEmails().size() >= 1) {
            intent.putExtra(ContactsContract.Intents.Insert.EMAIL, vc.getEmails().get(0).getValue());
        }

        //Phone number
        if (vc.getTelephoneNumbers().size() >= 1) {
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, vc.getTelephoneNumbers().get(0).getText());
        }

        //Address
        if (vc.getAddresses().size() >= 1) {
            intent.putExtra(ContactsContract.Intents.Insert.POSTAL, vc.getAddresses().get(0).getStreetAddressFull());
        }

    }

    private void parseResult(String result, String type) {
        //Website
        if (result.startsWith("http")) {
            State = ResultState.URL;
            setTitleBar("URL", getDateString() + type, R.drawable.ic_baseline_insert_link_24);
            setBody(result, "");
            setFooter(result);
        }

        //Contact
        else if (result.startsWith("BEGIN:VCARD")) {
            State = ResultState.CONTACT;
            setTitleBar("CONTACT", getDateString() + type, R.drawable.ic_baseline_person_add_24);

            //Parse VCARD
            VCard vcard = Ezvcard.parse(result).first();

            if (vcard.getStructuredName().getGiven() != null) {
                ResultObj = vcard;
                setBody(vcard.getStructuredName().getGiven(), generateContactString(vcard));
                setFooter(result);
            }
        }

        //Text
        else {
            State = ResultState.TEXT;
            setTitleBar("TEXT", getDateString() + type, R.drawable.ic_baseline_text_fields_24);
            setBody("Results", result);
            setFooter(result);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (result != null) {
            if (result.getContents() != null) {
                parseResult(result.getContents().toString(), result.getFormatName());
                return;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
